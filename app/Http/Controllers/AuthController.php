<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller
{
    
    public function login(Request $req){
        $email = $req->username;
        $password = $req->password;

        
        if (!empty($email) && !empty($password)){
            $user = User::where('email',$email)->first();
            
            if ($user){
                if (Hash::check($password, $user->password)) {
                    $token = $user->createToken('mytoken')->plainTextToken;
                    return ['user'=>$user,'token'=>$token];
                }else{
                    return ['status'=>'failed','message'=>'Wrong password'];
                }
            }else{
                return ['status'=>'failed','message'=>'User does not exist'];
            }
        }else{
            return ['status'=>'failed','message'=>'Please fill up username and password'];  
        }
    }

    public function logout(Request $req){
        $user = User::find($req->id);
        $user->tokens()->delete();
        return ['status'=>true,'message'=>'Logged out'];
    }

    public function register(Request $req){
        $muser = new User();

        $muser->save();
        return $muser;
    }

    public function hashPass(){
        return Hash::make('123456');
    }
}
